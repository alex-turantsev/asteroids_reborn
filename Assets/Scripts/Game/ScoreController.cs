﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace AT_Asteroids {
	public static class ScoreController {
		public enum ScoreType {
			Asteroid,

		};

		public static int HighScore{ get { return highScore; } }

		public static int Score{ get { return score; } }

		public delegate void UpdateValue (int score);

		public static event UpdateValue UpdateScore;

		private static int highScore;
		private static int score;
		private static int previousHighScore;

		private static Dictionary<ScoreType,int> scoreValues = new Dictionary<ScoreType, int> () {
			{ ScoreType.Asteroid, 10 }
		};

		static ScoreController () {
			highScore = PlayerPrefs.GetInt (Prefs.HighScore);
		}

		public static bool IsHighSCore {
			get{ return highScore > previousHighScore; }
		}

		public static void ResetScore () {
			score = 0;
			previousHighScore = highScore;
		}

		public static void AddScore (ScoreType type) {
			score += (int)((float)scoreValues [type] * Mathf.Pow (1.2f, LevelManager.CurrentWave));
			if (score > highScore) {
				highScore = score;
				PlayerPrefs.SetInt (Prefs.HighScore, highScore);
			}
			if (UpdateScore != null)
				UpdateScore (score);
		}
	}
}
