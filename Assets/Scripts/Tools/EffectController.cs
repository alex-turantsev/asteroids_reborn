﻿using UnityEngine;
using System.Collections;

public class EffectController : MonoBehaviour, IBehaviour {
	public float TimeToLive = 1f;

	private float m_tempTime = 0f;

	public void Init (Vector3 position = new Vector3 ()) {
		gameObject.SetActive (true);
		transform.position = position;
	}

	public void Init (Vector3 position, Quaternion rotation) {
		Init (position);
		transform.rotation = rotation;
	}

	#region IBehaviour implementation

	public void BeAwake () {
		throw new System.NotImplementedException ();
	}

	public void BeStart (GameObject go) {
		throw new System.NotImplementedException ();
	}

	public void BeUpdate () {
		m_tempTime += Time.deltaTime;
		if (m_tempTime > TimeToLive) {
			gameObject.SetActive (false);
		}
	}

	public bool IsActive { get { return gameObject.activeSelf; } }

	#endregion

	#region MonoBehaviour implementation
	private void OnEnable () {
		m_tempTime = 0f;
	}

	private void OnDisable () {
		
	}
	#endregion

}
