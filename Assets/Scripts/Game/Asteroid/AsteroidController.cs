﻿using UnityEngine;
using System.Collections;

namespace AT_Asteroids {
	public class AsteroidController : MonoBehaviour, IBehaviour {
		public enum ChildDirection {
			Left,
			Right,
		};

		public static int ActiveAsteroidCount = 0;

		public Sprite[] AstedoidsSprites;
		public int MaxAsteroidLevel = 3;
		public int AsteroidLevel = 0;
		public float Speed = 10f;
		public Transform SpriteObject;
		[Header ("")]
		public float SpeedScale = 1.2f;
		public float SizeScale = 0.6f;

		//components
		private Rigidbody2D m_rigidbody;
		private Transform m_transform;
		private CircleCollider2D m_collider;

		//inital values
		private float m_radius;
		private Vector3 m_spriteScale;

		private bool m_inited = false;
		private Vector3 m_direction;

		public void Init (AsteroidController parent = null, ChildDirection side = ChildDirection.Right, Vector3 direction = new Vector3 (), int level = 0) {
			m_inited = true;
			gameObject.SetActive (true);
			var speedScale = 1f;
			if (parent == null) {
				var spawn = AsteroidsSpawners.GetSpawn ();
				m_direction = spawn.direction;
				m_transform.position = spawn.position;
				AsteroidLevel = 0;
				SpriteObject.localScale = m_spriteScale;
				m_collider.radius = m_radius;
			} else {
				AsteroidLevel = level + 1;
				var scale = Mathf.Pow (SizeScale, AsteroidLevel);
				speedScale = Mathf.Pow (SpeedScale, AsteroidLevel);
				m_transform.position = parent.m_transform.position - direction * 0.5f;
				m_collider.radius = m_radius * scale;
				SpriteObject.localScale = m_spriteScale * scale;
				var sideDirection = direction;
				sideDirection.x = direction.y;
				sideDirection.y = direction.x;
				if (side == ChildDirection.Right) {
					sideDirection.y = -sideDirection.y;
				} else {
					sideDirection.x = -sideDirection.x;
				}
				m_direction = -direction + sideDirection;
			}
			SpriteObject.GetComponent<SpriteRenderer> ().sprite = AstedoidsSprites [UnityEngine.Random.Range (0, AstedoidsSprites.Length)];
			m_direction.Normalize ();
			m_transform.up = m_direction;
			m_rigidbody.velocity = m_direction * Speed * speedScale;
			m_rigidbody.angularVelocity = Random.Range (20f, 80f);
		}

		#region MonoBehaviour implementation
		private void Awake () {
			m_transform = transform;
			m_rigidbody = gameObject.GetComponent<Rigidbody2D> ();
			m_collider = gameObject.GetComponent < CircleCollider2D> ();
			m_spriteScale = SpriteObject.localScale;
			m_radius = m_collider.radius; 
		}

		private void OnEnable () {
			if (!m_inited && Debug.isDebugBuild) {
				Debug.LogError ("Asteroid not inited");
			}
			ActiveAsteroidCount++;
		}

		private void OnDisable () {
			m_inited = false;
			ActiveAsteroidCount--;
		}
		#endregion

		#region IBehaviour implementation
		public bool IsActive { get { return gameObject.activeSelf; } }

		public void BeAwake () {
			throw new System.NotImplementedException ();
		}

		public void BeStart (GameObject go) {
			throw new System.NotImplementedException ();
		}

		public void BeUpdate () {
			m_transform.KeepInCameraArea ();
		}
		#endregion

		private void OnTriggerEnter2D (Collider2D collider) {
			if (collider.gameObject.layer == Layers.Player || collider.gameObject.gameObject.layer == Layers.PlayerBullet) {
				if (collider.gameObject.gameObject.layer == Layers.PlayerBullet) {
					CameraShake.PlayAsteroidExplosion (AsteroidLevel + 1);
				}
				var direction = (collider.transform.position - m_transform.position).normalized;
				DestroyAsteroid (direction);
			}
		}

		private void DestroyAsteroid (Vector3 direction = new Vector3 ()) {
			InstantiatePickUp ();
			var effect = ObjectPool.GetAsteroidHitEffect ();
			effect.Init (m_transform.position);
			if (AsteroidLevel < MaxAsteroidLevel) {
				var ast = ObjectPool.GetAsteroid ();
				ast.Init (this, ChildDirection.Right, direction, AsteroidLevel);
				ast = ObjectPool.GetAsteroid ();
				ast.Init (this, ChildDirection.Left, direction, AsteroidLevel);
			}
			gameObject.SetActive (false);
		}

		private void InstantiatePickUp () {
			if (UnityEngine.Random.value < GameController.Instance.PickUpDropsPropability)
				Instantiate (Resources.Load<GameObject> ("PickUps/Life"), transform.position, Quaternion.identity);
		}
	}
}
