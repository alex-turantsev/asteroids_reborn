﻿using UnityEngine;
using System.Collections;

namespace AT_Asteroids {
	public class PlayerBullet : BaseBullet {
		private void OnTriggerEnter2D (Collider2D collider) {
			if (collider.gameObject.layer == Layers.Asteroid) {
				ScoreController.AddScore (ScoreController.ScoreType.Asteroid);
				gameObject.SetActive (false);
			}
		}
	}
}
