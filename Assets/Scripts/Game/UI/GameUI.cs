﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System;

namespace AT_Asteroids {
	public class GameUI : MonoBehaviour {
		public static GameUI Instance;

		public Button PauseButton;
		public PlayerLifesView PlayerLifes;
		public ScoreView Score;
		public PauseWindow Pause;
		public GameOverWindow GameOverScreen;
		[Header ("WaveNumber")]
		public Text WaveNumberText;
		public Animator WaveNumberAnimation;
		private readonly string WaveNumberTrigger = "Play";

		#region MonoBehaviour
		private void Awake () {
			Instance = this;
			Score.BeAwake ();
		}

		private void Start () {
			Score.BeStart (gameObject);
			Pause.Init ();
			GameOverScreen.Init ();
			PauseButton.onClick.AddListener (Pause.Open);
		}

		private void Update () {
			Score.BeUpdate ();
		}

		private void OnEnable () {
			Score.OnEnable ();
			LevelManager.SetWaveNumber += UpdateWaveNumber;
		}

		private void OnDisable () {
			Score.OnDisable ();
			LevelManager.SetWaveNumber -= UpdateWaveNumber;
		}
		#endregion

		private void UpdateWaveNumber (int wave) {
			WaveNumberText.text = wave.ToString ();
			WaveNumberAnimation.SetTrigger (WaveNumberTrigger);
		}

		public void GameOver () {
			GameOverScreen.Show ();
		}
	}

	[Serializable]
	public class PauseWindow {
		public GameObject Window;
		public Button CloseButton;
		public Button MenuButton;

		public void Init () {
			CloseButton.onClick.AddListener (Close);
			MenuButton.onClick.AddListener (LoadMenu);
			Close ();
		}

		public void Open () {
			Window.SetActive (true);
			GameController.State = GameController.GameState.Pause;
			Time.timeScale = 0f;
			if (BackGroundMusic.Instance != null)
				BackGroundMusic.Instance.Pause ();
		}

		private void Close () {
			Window.SetActive (false);
			GameController.State = GameController.GameState.Game;
			Time.timeScale = 1f;
			if (BackGroundMusic.Instance != null)
				BackGroundMusic.Instance.UnPause ();
		}

		private void LoadMenu () {
			Time.timeScale = 1f;
			if (BackGroundMusic.Instance != null)
				BackGroundMusic.Instance.UnPause ();
			SceneScontroller.LoadScene (Scenes.Menu);
		}
	}

	[Serializable]
	public class GameOverWindow {
		public GameObject Parent;
		public Button Menu;
		public Button Replay;
		public Text NewRecord;

		public void Init () {
			Parent.SetActive (false);
			Menu.onClick.AddListener (LoadMenu);
			Replay.onClick.AddListener (ReplayGame);
		}

		public void Show () {
			Parent.SetActive (true);
			NewRecord.gameObject.SetActive (ScoreController.IsHighSCore);
			Parent.GetComponent<Animation> ().Play ();
		}

		private void LoadMenu () {
			SceneScontroller.LoadScene (Scenes.Menu);
		}

		private void ReplayGame () {
			SceneManager.LoadScene (Scenes.Game);
		}
			
	}
}
