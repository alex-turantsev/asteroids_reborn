﻿using UnityEngine;
using System.Collections;
using System;

namespace AT_Asteroids.Player {
	[Serializable]
	public class PlayerWeapon : IBehaviour {
		public float FireRate = 0.6f;
		public float BulletSpeed = 10f;
		public Transform ShootingPoint;
		public AudioSource ShootSource;
		public bool IsShooting = true;

		private float m_lastShoot = 0f;

		#region IBehaviour implementation

		public void BeAwake () {
			
		}

		public void BeStart (GameObject go) {
			
		}

		public void BeUpdate () {
			if (IsShooting)
				Shooting ();
		}

		public bool IsActive { get { return true; } }
		#endregion

		private void Shooting () {
			if (m_lastShoot < Time.time) {
				m_lastShoot = FireRate + Time.time;
				var bullet = ObjectPool.GetPlayerBullet ();
				bullet.GetComponent<BaseBullet> ().Init (ShootingPoint.position + 0.1f * ShootingPoint.right, ShootingPoint.up, BulletSpeed);
				ShootSource.Play ();
			}
		}

	}
}
