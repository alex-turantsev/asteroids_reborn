﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

namespace AT_Asteroids {
	public class MenuUI : MonoBehaviour {
		public Transform Buttons;
		public Text HighScoreText;

		private void Start () {
			HighScoreText.text = PlayerPrefs.GetInt (Prefs.HighScore).ToString ("000000000");
		}

		public void Load () {
			SceneScontroller.LoadScene (Scenes.Game);
		}
	}
}
