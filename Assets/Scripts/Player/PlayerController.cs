﻿using UnityEngine;
using System.Collections;

namespace AT_Asteroids.Player {
	public class PlayerController : MonoBehaviour {
		public static PlayerController Instance;
		public SpriteRenderer SpriteObject;
		public PlayerMovement Movement;
		public PlayerWeapon Weapon;
		public PlayerHealth Health;
		public PlayerAnimation Animation;

		public float ReviveTime = 2f;
		public float BeforeReviveTime = 2f;
		public GameObject Explosion;

		public bool IsAlive{ get { return m_isAlive; } }

		private bool m_isAlive = true;
		private IBehaviour[] m_behaviours;
		private Collider2D m_collider;

		#region MonoBehaviour
		private void Awake () {
			Instance = this;
			m_behaviours = new IBehaviour[]{ Movement, Weapon };
			for (var i = 0; i < m_behaviours.Length; i++) {
				m_behaviours [i].BeAwake ();
			}
			m_collider = GetComponent<Collider2D> ();
			SetAlive (false);
		}

		private void Start () {
			Health.Init ();
			for (var i = 0; i < m_behaviours.Length; i++) {
				m_behaviours [i].BeStart (gameObject);
			}
		}

		public void StartGame () {
			SetAlive (true);
		}

		private void Update () {
			if (m_isAlive) {
				for (var i = 0; i < m_behaviours.Length; i++) {
					m_behaviours [i].BeUpdate ();
				}
			}
		}

		private void FixedUpdate () {
			if (m_isAlive)
				Movement.BeFixedUpdate ();
		}

		private void OnTriggerEnter2D (Collider2D collider) {
			if (!m_isAlive)
				return;
			if (collider.gameObject.layer == Layers.Asteroid) {
				SetAlive (false);
				var lifesRemain = Health.ReduceLife ();
				Death ();
				if (lifesRemain) {
					StartCoroutine (NewLife ());
				} else {
					GameController.Instance.GameOver ();
					GameUI.Instance.GameOver ();
				}
				return;
			}
			if (collider.gameObject.layer == Layers.PickUp) {
				Health.AddLife ();
				Destroy (collider.gameObject);
			}
		}
		#endregion MonoBehaviour

		private void SetAlive (bool alive) {
			m_collider.enabled = alive;
			m_isAlive = alive;
		}

		private IEnumerator NewLife () {
			yield return new WaitForSeconds (BeforeReviveTime);
			transform.position = Vector3.zero;
			SpriteObject.enabled = true;

			StartCoroutine (ImmortalState ());
		}

		private IEnumerator ImmortalState () {
			Weapon.IsShooting = false;
			Animation.SetRevive ();
			m_isAlive = true;
			yield return new WaitForSeconds (ReviveTime);
			Weapon.IsShooting = true;
			Animation.SetDefault ();
			SetAlive (true);
		}

		private void Death () {
			SpriteObject.enabled = false;
			var go = Instantiate (Explosion, transform.position, Quaternion.identity);
			Destroy (go, 3);
			CameraShake.PlayPlayerExplosion ();
		}
	}
}
