﻿using UnityEngine;
using System.Collections;

namespace AT_Asteroids {
	public static class Layers {
		public static readonly string PlayerS = "Player";
		public static readonly int Player = LayerMask.NameToLayer (PlayerS);

		public static readonly string PlayerBulletS = "PlayerBullet";
		public static readonly int PlayerBullet = LayerMask.NameToLayer (PlayerBulletS);

		public static readonly string AsteroidS = "Asteroid";
		public static readonly int Asteroid = LayerMask.NameToLayer (AsteroidS);

		public static readonly string PickUpS = "PickUp";
		public static readonly int PickUp = LayerMask.NameToLayer (PickUpS);
	}

	public static class Tags {

	}

	public static class Prefs {
		public static readonly string HighScore = "highScore";
	}

	public static class Scenes {
		public static readonly string Game = "Game";
		public static readonly string Menu = "Menu";
		public static readonly string Loading = "Loading";
	}
}
