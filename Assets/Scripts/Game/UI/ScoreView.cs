﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

namespace AT_Asteroids {
	[Serializable]
	public class ScoreView :IBehaviour {
		public float UpdatingSpeed = 5f;
		public Text ScoreText;
		public Animation ScoteTextAnim;

		private int m_lastScore = 0;
		private int m_currentScore = 0;
		private int m_destScore = 0;
		private bool m_isUpdating = false;
		private float m_tempTime;

		#region IBehaviour implementation

		public void BeAwake () {
			ScoreText.text = 0.ToString ("000000000");
			m_lastScore = 0;
			m_currentScore = 0;
			m_destScore = 0;
			m_isUpdating = false;
		}

		public void BeStart (GameObject go) {
			ScoreController.ResetScore ();
		}

		public void BeUpdate () {
			if (m_isUpdating) {
				m_tempTime += Time.deltaTime * UpdatingSpeed;
				if (m_tempTime > 1f)
					m_isUpdating = false;
				m_currentScore = (int)Mathf.Lerp (m_lastScore, m_destScore, m_tempTime);
				ScoreText.text = m_currentScore.ToString ("000000000");
			}
		}

		public bool IsActive { get { return true; } }

		#endregion

		private void UpdateScore (int score) {
			if (score >= 1000000000)
				return;
			ScoteTextAnim.Play ();
			m_isUpdating = true;	
			m_lastScore = m_currentScore;
			m_destScore = score;
			m_tempTime = 0f;
		}

		public void OnEnable () {
			ScoreController.UpdateScore += UpdateScore;
		}

		public void OnDisable () {
			ScoreController.UpdateScore -= UpdateScore;
		}

	}
}
