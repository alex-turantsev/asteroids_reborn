﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

public class BackGroundMusic : MonoBehaviour {
	public static BackGroundMusic Instance = null;
	public float FadeInSeconds = 2f;
	public AudioMixerGroup NormalMixer;
	public AudioMixerGroup PauseMixer;
	private AudioSource m_source;

	private void Awake () {
		if (Instance != null) {
			Destroy (gameObject);
		} else {
			Instance = this;
		}
		DontDestroyOnLoad (gameObject);
		m_source = GetComponent<AudioSource> ();
	}

	private void Start () {
		
		StartCoroutine (FadeInMusic ());
		m_source.outputAudioMixerGroup = NormalMixer;
	}

	private IEnumerator FadeInMusic () {
		float tempTime = 0f;
		while (tempTime <= 1f) {
			tempTime += Time.deltaTime / FadeInSeconds;
			m_source.volume = tempTime;
			yield return null;
		}
	}

	public void Pause () {
		m_source.outputAudioMixerGroup = PauseMixer;
	}

	public void UnPause () {
		m_source.outputAudioMixerGroup = NormalMixer;
	}

}
