﻿using UnityEngine;
using System.Collections;
using System;

namespace AT_Asteroids {
	public class CameraShake : MonoBehaviour {
		private static CameraShake Instance;

		public bool IsShaking = false;
		private Vector3 m_strength = new Vector3 (2f, 2f, 0f);
		private float m_decay = 0.8f;
		private float m_shakeTime = 1f;
		private Vector3 m_cameraOrigin;

		public void Start () {
			Instance = this;
			m_cameraOrigin = Camera.main.transform.position;
		}

		public static void PlayPlayerExplosion () {
			if (Instance != null)
				Instance.StartShake (Vector3.one * 1.5f, 0.8f, 1f);
		}

		public static void PlayAsteroidExplosion (int level) {
			if (Instance != null)
				Instance.StartShake (Vector3.one / level, 0.8f, 1f);
		}

		public void Update () {
			if (IsShaking == true) {
				if (m_shakeTime > 0) {		
					m_shakeTime -= Time.deltaTime;

					Vector3 tempPosition = Camera.main.transform.position;

					tempPosition.x = m_cameraOrigin.x + UnityEngine.Random.Range (-m_strength.x, m_strength.x);
					tempPosition.y = m_cameraOrigin.y + UnityEngine.Random.Range (-m_strength.y, m_strength.y);

					Camera.main.transform.position = tempPosition;

					m_strength *= m_decay;
				} else if (Camera.main.transform.position != m_cameraOrigin) {
					m_shakeTime = 0;

					Camera.main.transform.position = m_cameraOrigin;

					IsShaking = false;
				}
			}
		}

		public void StartShake (Vector3 strength, float decay, float shakeTime) {
			m_shakeTime = shakeTime;
			IsShaking = true;
			m_strength = strength;
			m_decay = decay;
		}
	}
}
