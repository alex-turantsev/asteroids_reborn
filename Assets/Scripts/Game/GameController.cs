﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using AT_Asteroids.Player;

namespace AT_Asteroids {
	public class GameController : MonoBehaviour {
		public enum GameState {
			Game,
			Pause,
		};

		public static GameController Instance;
		public static GameState State = GameState.Game;
		public LevelManager Level;
		public Parallax Parallax;
		public float PickUpDropsPropability = 0.08f;

		private void Awake () {
			Instance = this;
			Level.BeAwake ();
		}

		private IEnumerator Start () {
			yield return new WaitForSeconds (2f);
			Player.PlayerController.Instance.StartGame ();
			yield return new WaitForSeconds (2f);
			Level.BeStart (gameObject);
		}

		public void GameOver () {
			
		}

		private void Update () {
			Level.BeUpdate ();
			Parallax.Update ();
		}
	}

	[Serializable]
	public class Parallax {
		public float ParallaxStrength = 0.2f;
		public Transform ParallaxParent;

		public void Update () {
			for (var index = 0; index < ParallaxParent.childCount; index++) {	
				var tempPosition = ParallaxParent.GetChild (index).position;
				var pltrans = PlayerController.Instance.transform;
				// Move the starfield opposite the player's movement, limited by the size of the starfield, to give a feeling of parallax movement
				tempPosition.x = -pltrans.position.x * ParallaxStrength * 1 / (index + 1);
				tempPosition.y = -pltrans.position.y * ParallaxStrength * 1 / (index + 1);

				ParallaxParent.GetChild (index).position = Vector3.Lerp (ParallaxParent.GetChild (index).position, tempPosition, 2f * Time.deltaTime);
			}
		}
	}
}
