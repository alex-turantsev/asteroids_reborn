﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

namespace AT_Asteroids {
	public class LoadingContoller : MonoBehaviour {
		private AsyncOperation m_async;

		private void Start(){
			StartCoroutine(LoadScene());
		}

		private IEnumerator LoadScene(){
			m_async = SceneManager.LoadSceneAsync (SceneScontroller.NextSceneName);
			m_async.allowSceneActivation = false;

			while (m_async.progress < 0.9f) {
				yield return  null;
			}
			yield return new WaitForSeconds(2f);
			m_async.allowSceneActivation = true;
		}
	}
}
