﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

namespace AT_Asteroids {
	[Serializable]
	public class PlayerLifesView {
		public Transform LifesGridTransform;
		public GameObject LifePrefab;
		private List<GameObject> m_lifes = new List<GameObject> ();

		public void Init (int lifes) {
			if (m_lifes.Count > 0) {
				foreach (var life in m_lifes) {
					MonoBehaviour.Destroy (life);
				}
				m_lifes.Clear ();
			}
			for (var i = 0; i < lifes; i++) {
				var go = MonoBehaviour.Instantiate (LifePrefab);
				go.transform.SetParent (LifesGridTransform, false);
				m_lifes.Add (go);
			}
		}

		public void AddLife () {
			var go = MonoBehaviour.Instantiate (LifePrefab);
			go.transform.SetParent (LifesGridTransform, false);
			m_lifes.Add (go);

		}

		public void ReduceLife () {
			MonoBehaviour.Destroy (m_lifes [m_lifes.Count - 1]);
			m_lifes.RemoveAt (m_lifes.Count - 1);
		}
	}
}
