﻿using UnityEngine;

interface IBehaviour {
	bool IsActive{ get; }

	void BeAwake ();

	void BeStart (GameObject go);

	void BeUpdate ();
}
