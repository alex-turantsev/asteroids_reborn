﻿using UnityEngine;
using System.Collections;
using System;

namespace AT_Asteroids.Player {
	[Serializable]
	public class PlayerAnimation {
		public Animator Animator;
		private readonly string DefaultTrigger = "SetDefault";
		private readonly string ReviveTrigger = "SetRevive";

		public void SetDefault () {
			Animator.SetTrigger (DefaultTrigger);
		}

		public void SetRevive () {
			Animator.SetTrigger (ReviveTrigger);
		}
	}
}
