﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace AT_Asteroids {
	public class ObjectPool : MonoBehaviour {
		private enum ObjectTypes {
			Asteroid,
			PlayerBullet,
		};

		public static ObjectPool Instance;

		public GameObject PlayerBulletPrefab;
		public Transform PlayerBulletContainer;
		private List<PlayerBullet> m_plBullets = new List<PlayerBullet> ();

		public GameObject AsteroidHitEffect;
		public GameObject EdgeEffect;
		public Transform EffectsContainer;
		private List<EffectController> m_effects = new List<EffectController> ();
		private List<EffectController> m_EdgeEffects = new List<EffectController> ();

		public GameObject AsteroidPrefab;
		public Transform AsteroidContainer;
		private List<AsteroidController> m_asterroids = new List<AsteroidController> ();


		private Dictionary<int,List<GameObject>> objectsDictionary = new Dictionary<int, List<GameObject>> ();
		private List<IBehaviour> behaviors = new List<IBehaviour> ();

		private void Start () {
			Instance = this;
		}

		#region static methods
		public static AsteroidController GetAsteroid () {
			if (Instance != null) {
				return Instance.GetMonobehaviorObject<AsteroidController> (Instance.m_asterroids, Instance.AsteroidPrefab, Instance.AsteroidContainer, true);
			}
			return null;
		}

		public static EffectController GetAsteroidHitEffect () {
			if (Instance != null) {
				return Instance.GetMonobehaviorObject<EffectController> (Instance.m_effects, Instance.AsteroidHitEffect, Instance.EffectsContainer, true);
			}
			return null;
		}


		public static EffectController GetEdgeEffect () {
			if (Instance != null) {
				return Instance.GetMonobehaviorObject<EffectController> (Instance.m_EdgeEffects, Instance.EdgeEffect, Instance.EffectsContainer, true);
			}
			return null;
		}

		public static PlayerBullet GetPlayerBullet () {
			if (Instance != null) {
				return Instance.GetMonobehaviorObject<PlayerBullet> (Instance.m_plBullets, Instance.PlayerBulletPrefab, Instance.PlayerBulletContainer, true);
			}
			return null;
		}
		#endregion

		private T GetMonobehaviorObject<T> (List<T> list, GameObject prefab, Transform container, bool addToUpdate = false) where T : MonoBehaviour {
			T obj = null;
			for (var i = 0; i < list.Count; i++) {
				var item = list [i];
				if (item != null && !item.gameObject.activeSelf) {
					obj = item;
					break;
				}
			}
			if (obj == null) {
				obj = Instantiate (prefab).GetComponent<T> ();
				obj.transform.SetParent (container);
				list.Add (obj);
				if (addToUpdate && (obj is IBehaviour)) {
					behaviors.Add ((IBehaviour)obj);
				}
			}
			return obj;
		}

		private GameObject GetObject (GameObject prefab, Transform container) {
			var prefabID = prefab.GetInstanceID ();
			if (objectsDictionary.ContainsKey (prefabID) == false) {
				objectsDictionary.Add (prefabID, new List<GameObject> ());
			}
			var list = objectsDictionary [prefabID];
			GameObject go = null;
			for (var i = 0; i < list.Count; i++) {
				var item = list [i];
				if (item != null && !item.activeSelf) {
					go = item;
					break;
				}
			}
			if (go == null) {
				go = Instantiate (prefab);
				go.transform.SetParent (container);
				list.Add (go);
			}
			go.SetActive (true);
			return go;
		}

		private void Update () {
			for (var i = 0; i < behaviors.Count; i++) {
				if (behaviors [i].IsActive)
					behaviors [i].BeUpdate ();
			}
		}
	}
		
}

