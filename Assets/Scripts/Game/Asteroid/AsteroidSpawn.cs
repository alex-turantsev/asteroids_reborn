﻿using UnityEngine;
using System.Collections;

namespace AT_Asteroids {
	public struct AsteroidSpawn {
		public Vector3 position;
		public Vector3 direction;
	}

	public static class AsteroidsSpawners {
		public static AsteroidSpawn GetSpawn () {
			AsteroidSpawn spawn;
			spawn.direction = Vector3.zero;
			var pos = Camera.main.WorldToViewportPoint (Vector3.zero);
			pos = RandomViewPortOnEdge (pos);
			spawn.position = Camera.main.ViewportToWorldPoint (pos);
			spawn.direction = RandomViewPortDirection (pos);
			return spawn;
		}

		private static Vector3 RandomViewPortDirection (Vector3 viewPortpos) {
			var targerPos = RandomCenterPoint ();
			targerPos.z = viewPortpos.z;
			return (targerPos - viewPortpos).normalized;
		}

		private static Vector3 RandomViewPortOnEdge (Vector3 viewPortPos) {
			if (UnityEngine.Random.value > 0.5f) {
				viewPortPos.x = RandomEdge ();
				viewPortPos.y = UnityEngine.Random.value;
			} else {
				viewPortPos.x = UnityEngine.Random.value;
				viewPortPos.y = RandomEdge ();
			}
			return viewPortPos;
		}

		private static float RandomEdge () {
			return (UnityEngine.Random.value > 0.5f) ? 0.02f : 0.98f;
		}

		private static Vector3 RandomCenterPoint () {
			Vector3 pos;
			float centerLimit = 0.35f;
			pos.x = UnityEngine.Random.Range (centerLimit, 1f - centerLimit);
			pos.y = UnityEngine.Random.Range (centerLimit, 1f - centerLimit);
			pos.z = 0f;
			return pos;
		}
	}
}
