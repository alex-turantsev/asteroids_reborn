﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

namespace AT_Asteroids {
	public static class SceneScontroller{
		public static string NextSceneName{
			get{
				return string.IsNullOrEmpty (nextSceneName) ? Scenes.Menu : nextSceneName;
			}
		}
		private static string nextSceneName;

		public static void LoadScene(string name){
			nextSceneName = name;
			SceneManager.LoadScene (Scenes.Loading);
		}
	}
}