﻿using UnityEngine;
using System.Collections;
using System;

namespace AT_Asteroids {
	[Serializable]
	public class LevelManager : IBehaviour {
		public enum LevelManagerState {
			None,
			Spawning,
			WaitingForKill,
			DelayBeforeLevel,
		};

		public static int CurrentWave{ get { return Mathf.Clamp (m_currentWave + 1, 1, int.MaxValue); } }

		public static event SetInt SetWaveNumber;

		public float WavesDelay = 3f;
	
		private static int m_currentWave = 0;
		private LevelManagerState m_state;
		private StateFunctionDelegate m_stateFunction;
		private float m_tempTime;

		private delegate void StateFunctionDelegate ();

		public delegate void SetInt (int value);

		#region IBehaviour implementation

		public void BeAwake () {
			m_stateFunction = EmptyState;
			m_currentWave = 0;
		}

		public void BeStart (GameObject go) {
			SpawnAndSetWaitingState ();
		}

		public void BeUpdate () {
			m_stateFunction ();
		}

		public bool IsActive { get { return true; } }
		#endregion

		#region States
		private void EmptyState () {
		}

		private void WaitingForKillState () {
			if (AsteroidController.ActiveAsteroidCount < 1) {
				SetDelayState ();
			}
		}

		private void SetDelayState () {
			m_tempTime = Time.time;
			m_stateFunction = DelayBeforeWave;
		}

		private void DelayBeforeWave () {
			if ((m_tempTime + WavesDelay) < Time.time) {
				m_currentWave++;
				SpawnAndSetWaitingState ();
				m_stateFunction = EmptyState;
			}
		}

		private void SpawnAndSetWaitingState () {
			if (SetWaveNumber != null) {
				SetWaveNumber (m_currentWave + 1);
			}
			GameController.Instance.StartCoroutine (SpawnEnemies ());
		}
		#endregion

		private IEnumerator SpawnEnemies () {
			yield return new WaitForSeconds (2f);
			var count = m_currentWave + 1;
			for (var i = 0; i < count; i++) {
				var ast = ObjectPool.GetAsteroid ();
				ast.Init ();
			}
			m_stateFunction = WaitingForKillState;
		}
			
	}
}
