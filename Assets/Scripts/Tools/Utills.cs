﻿using UnityEngine;
using System.Collections;

namespace AT_Asteroids {
	public static class Utills {
		public static readonly float SpawnScreenOffset = 0.3f;

		public delegate void LimitEnter (Vector3 oldPosition, Vector3 newPosition);

		/// <summary>
		/// Keep transform in camera area, that if it trying to escape from one side, it will appear on oposite side
		/// </summary>
		/// <param name="transform">Transform.</param>
		public static void KeepInCameraArea (this Transform transform, LimitEnter limitEnter = null) {
			var minLimit = 0.01f;
			var maxLimit = 0.99f;
			var viewPortPoint = Camera.main.WorldToViewportPoint (transform.position);
			if (viewPortPoint.x < minLimit || viewPortPoint.x > maxLimit) {
				var pos = transform.position;
				pos.x = -pos.x;
				pos.x += SpawnScreenOffset * (viewPortPoint.x > maxLimit ? 1 : -1);
				if (limitEnter != null) {
					limitEnter (transform.position, pos);
				}
				transform.position = pos;
			}

			if (viewPortPoint.y < minLimit || viewPortPoint.y > maxLimit) {
				var pos = transform.position;
				pos.y = -pos.y;
				pos.y += SpawnScreenOffset * (viewPortPoint.y > maxLimit ? 1 : -1);
				if (limitEnter != null) {
					limitEnter (transform.position, pos);
				}
				transform.position = pos;
			}
		}
			
	}


}
