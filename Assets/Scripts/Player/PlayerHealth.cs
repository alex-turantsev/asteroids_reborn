﻿using UnityEngine;
using System.Collections;
using System;

namespace AT_Asteroids.Player {
	[Serializable]
	public class PlayerHealth {
		public int Lifes = 3;
		public bool Immortal = false;

		private PlayerLifesView m_lifeUI;

		public void Init () {
			m_lifeUI = GameUI.Instance.PlayerLifes;
			m_lifeUI.Init (Lifes);
		}

		/// <summary>
		/// Reduces the life.
		/// </summary>
		/// <returns><c>true</c>, if lifes remain<c>false</c> otherwise.</returns>
		public bool ReduceLife () {
			if (Immortal)
				return true;
			m_lifeUI.ReduceLife ();
			return (--Lifes) > 0;
		}

		public void AddLife () {
			if (Lifes < 5) {
				Lifes++;
				m_lifeUI.AddLife ();
			}	
		}
	}
}
