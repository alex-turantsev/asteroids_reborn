﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using CnControls;
using System;

namespace AT_Asteroids.Player {
	[Serializable]
	public class PlayerMovement :IBehaviour {
		public float Speed = 5f;

		private Rigidbody2D m_rigidbody;
		private Transform m_transform;

		#region IBehaviour implementation
		public bool IsActive { get { return true; } }

		public void BeAwake () {

		}

		public void BeStart (GameObject go) {
			m_rigidbody = go.GetComponent<Rigidbody2D> ();
			m_transform = go.transform;
		}

		public void BeUpdate () {
			//keeping player inside camera
			m_transform.KeepInCameraArea (SetEdgeEffect);
		}
		#endregion

		private void SetEdgeEffect (Vector3 oldPosition, Vector3 newPosition) {
			var changeAngle = Mathf.Abs (oldPosition.x - newPosition.x) > Mathf.Abs (oldPosition.y - newPosition.y);
			var go = ObjectPool.GetEdgeEffect ();
			go.Init (oldPosition, Quaternion.Euler (0f, 0f, changeAngle ? 90f : 0f));
			go = ObjectPool.GetEdgeEffect ();
			go.Init (newPosition, Quaternion.Euler (0f, 0f, changeAngle ? 90f : 0f));
		}

		private Vector2 m_tempDirection;

		public void BeFixedUpdate () {
			m_tempDirection.x = CnInputManager.GetAxis ("Horizontal");
			m_tempDirection.y = CnInputManager.GetAxis ("Vertical");
			if (m_tempDirection.magnitude > 0.1f) {
				m_rigidbody.AddForce (m_tempDirection * Speed * Time.fixedDeltaTime * 10);
				m_tempDirection.x *= -1;
				m_transform.eulerAngles = new Vector3 (0f, 0f, Mathf.Atan2 (m_tempDirection.x, m_tempDirection.y) * Mathf.Rad2Deg);
			}
		}
	}
}
