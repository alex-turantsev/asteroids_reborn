﻿using UnityEngine;
using System.Collections;

namespace AT_Asteroids {
	public class BaseBullet : MonoBehaviour , IBehaviour {
		public float LifeTime = 1f;
		protected float AwakeTime;

		protected Rigidbody2D m_rigidbody;
		protected Transform m_transform;

		#region IBehaviour implementation

		public virtual void BeAwake () {
			throw new System.NotImplementedException ();
		}

		public virtual void BeStart (GameObject go) {
			throw new System.NotImplementedException ();
		}

		public virtual void BeUpdate () {
			if (AwakeTime < (Time.time - LifeTime))
				gameObject.SetActive (false);
			m_transform.KeepInCameraArea ();
		}

		public virtual bool IsActive { get { return gameObject.activeSelf; } }

		#endregion

		protected virtual void Awake () {
			m_rigidbody = GetComponent<Rigidbody2D> ();
			m_transform = transform;
		}

		protected virtual void OnEnable () {
			AwakeTime = Time.time;
		}

		public virtual void Init (Vector3 position, Vector3 direction, float speed) {
			gameObject.SetActive (true);
			m_transform.right = direction;
			m_transform.position = position;
			m_rigidbody.velocity = direction * speed;
		}
	}
}
